#include <iostream>
#include <Eigen/Dense>
#include <ctime>
// Multiplication de Strassen

/** NOTICE 
 * compilation : g++ -Wall -O2 -I /usr/include/eigen3 exo1-2.cpp -o exo1-2.out
 * execution : ./exo1-2.out 
 */


Eigen::MatrixXd matrixProduct(const Eigen::MatrixXd &m1, const Eigen::MatrixXd &m2){
    
    Eigen::MatrixXd product = Eigen::MatrixXd::Zero(m1.rows(),m2.cols());
    if(m1.cols()==m2.rows()){

        for(int i=0;i<m1.rows();i++)
            for(int j=0;j<m2.cols();j++)
                for (int k=0; k<m1.cols(); k++)
                    product(i,j)+= m1(i,k) * m2(k,j);

    }else
        std::cerr << "OnO c'est pas la bonne taille !" << std::endl;
    return product;Eigen::MatrixXd result = Eigen::MatrixXd::Zero(2,2); 
}

Eigen::MatrixXd strassenProduct(const Eigen::MatrixXd &m1, const Eigen::MatrixXd &m2){
    int size = m1.cols();

    if (size <50)
        return m1*m2;

    int middle = size/2;
    Eigen::MatrixXd result = Eigen::MatrixXd::Zero(size,size); 

    Eigen::MatrixXd A = m1.topLeftCorner(middle,middle);
    Eigen::MatrixXd B = m1.topRightCorner(middle,middle);
    Eigen::MatrixXd C = m1.bottomLeftCorner(middle,middle);
    Eigen::MatrixXd D = m1.bottomRightCorner(middle,middle);
    Eigen::MatrixXd E = m2.topLeftCorner(middle,middle);
    Eigen::MatrixXd F = m2.topRightCorner(middle,middle);
    Eigen::MatrixXd G = m2.bottomLeftCorner(middle,middle);
    Eigen::MatrixXd H = m2.bottomRightCorner(middle,middle);

    Eigen::MatrixXd P1 = strassenProduct(A, F-H);
    Eigen::MatrixXd P2 = strassenProduct(A+B, H);
    Eigen::MatrixXd P3 = strassenProduct(C+D, E);
    Eigen::MatrixXd P4 = strassenProduct(D,G-E);
    Eigen::MatrixXd P5 = strassenProduct(A+D, E+H);
    Eigen::MatrixXd P6 = strassenProduct(B-D, G+H);
    Eigen::MatrixXd P7 = strassenProduct(A-C, E+F);

    result.topLeftCorner(middle,middle)=P5+P4-P2+P6;
    result.topRightCorner(middle,middle)=P1+P2;
    result.bottomLeftCorner(middle,middle)=P3+P4;
    result.bottomRightCorner(middle,middle)=P1+P5-P3-P7;

    return result;
}

int main(){
    const unsigned int size  = 2048;
    const unsigned int nbRuns = 5;

    Eigen::MatrixXd A = Eigen::MatrixXd::Random(size, size);
    Eigen::MatrixXd B = Eigen::MatrixXd::Random(size, size);
    Eigen::MatrixXd C = Eigen::MatrixXd::Random(size, size);

    std::cout << "Test de performance des produits matriciels : "<<std::endl;
    // std::cout << "Strassen : "<< strassenProduct(A,B) << std::endl;
    // std::cout << "Eiden : " << A*B << std::endl; 

    std::cout << "strassen start" << std::endl;
    clock_t begin = clock();
    for(unsigned int count=0; count<nbRuns; ++count)
        C = strassenProduct(A,B);
    clock_t end = clock();
    double tempsCalc = double(end - begin) / CLOCKS_PER_SEC;
    std::cout << "Strassen :   " <<tempsCalc <<"s " << std::endl;

    std::cout << "eigen start" << std::endl;
    begin = clock();
    for(unsigned int count=0; count<nbRuns; ++count)
        C = A*B;
    end = clock();
    tempsCalc = double(end - begin) / CLOCKS_PER_SEC;
    std::cout << "Eigen : " <<tempsCalc / 8 <<"s " << std::endl;

    std::cout << "loops start" << std::endl;
    begin = clock();
    for(unsigned int count=0; count<nbRuns; ++count)
        C = matrixProduct(A,B);
    end = clock();
    tempsCalc = double(end - begin) / CLOCKS_PER_SEC;
    std::cout << "Notre : " <<tempsCalc <<"s " << std::endl;


    return 0;
}