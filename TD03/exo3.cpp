#include <iostream>
#include <Eigen/Dense>

// Methode de vérification 

/** NOTICE 
 * compilation : g++ -Wall -O2 -I /usr/include/eigen3 exo3.cpp -o exo3.out
 * execution : ./exo3.out 
 */

int verifDotProduct(Eigen::MatrixXd A,Eigen::MatrixXd B,Eigen::MatrixXd C){
    Eigen::VectorXd b = B.rowwise().sum();
    Eigen::VectorXd c = C.rowwise().sum();
    Eigen::VectorXd result = A*b-c; 
    for (unsigned int i = 0; i < result.size(); i++){
        if(result[i]>10e-13)
            return 0;
    }
    return 1;
}

int main(){
    const unsigned int size  = 500;
    Eigen::MatrixXd A = Eigen::MatrixXd::Random(size, size);
    Eigen::MatrixXd B = Eigen::MatrixXd::Random(size, size);
    Eigen::MatrixXd C = A*B;

    if (verifDotProduct(A,B,C))
        std::cout<<"A*B=C produit juste"<<std::endl;
    else
        std::cout<<"A*B=C erreur de calcul"<<std::endl;

    return 0;
}