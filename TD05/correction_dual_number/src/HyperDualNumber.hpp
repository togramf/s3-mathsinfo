#pragma once

#include <fstream>
#include <cmath>
#include <limits>

// see code : http://adl.stanford.edu/hyperdual/hyperdual.h
// slides   : https://www.osti.gov/servlets/purl/1368722

template <typename T>
class HyperDualNumber{

	private : 
		T _real;
		T _dual1;
		T _dual2;
		T _dual12;
		
	public :
		HyperDualNumber() = default;
		HyperDualNumber(const T& real, const T &dual1, const T &dual2, const T &dual12) : _real(real), _dual1(dual1), _dual2(dual2), _dual12(dual12) {};
		~HyperDualNumber() = default;

		inline T & real()   {return _real;};
		inline T & dual1()  {return _dual1;};
		inline T & dual2()  {return _dual2;};
		inline T & dual12() {return _dual12;};
		inline const T & real()   const {return _real;};
		inline const T & dual1()  const {return _dual1;};
		inline const T & dual2()  const {return _dual2;};
		inline const T & dual12() const {return _dual12;};

		HyperDualNumber operator+(const HyperDualNumber &hdn) const;
		HyperDualNumber operator-(const HyperDualNumber &hdn) const;
		HyperDualNumber operator*(const HyperDualNumber &hdn) const;
		HyperDualNumber operator/(const HyperDualNumber &hdn) const;
		HyperDualNumber operator*(const T &value) const;
		HyperDualNumber operator/(const T &value) const;
        inline friend HyperDualNumber operator*(const T &value, const HyperDualNumber &hdn) {return hdn*value;};
        inline friend HyperDualNumber operator/(const T &value, const HyperDualNumber &hdn) {return HyperDualNumber(value,1,1,0)/hdn;};
        inline friend HyperDualNumber operator-(const HyperDualNumber &hdn) {return HyperDualNumber(-hdn._real,-hdn._dual1,-hdn._dual2,-hdn._dual12);}; // unary minus

		static HyperDualNumber inv(const HyperDualNumber & hdn);
		static HyperDualNumber sin(const HyperDualNumber & hdn);
		static HyperDualNumber cos(const HyperDualNumber & hdn);
		static HyperDualNumber tan(const HyperDualNumber & hdn);
		static HyperDualNumber exp(const HyperDualNumber & hdn);
		static HyperDualNumber log(const HyperDualNumber & hdn);
		static HyperDualNumber abs(const HyperDualNumber & hdn);
		static HyperDualNumber pow(const HyperDualNumber & hdn, const T &n);
		static HyperDualNumber sqrt(const HyperDualNumber & hdn);

		template<typename U>
        friend std::ostream& operator<< (std::ostream& stream, const HyperDualNumber<U> &hdn);
};

template <typename T>
HyperDualNumber<T> HyperDualNumber<T>::operator+(const HyperDualNumber &hdn) const {
	return HyperDualNumber(this->_real + hdn._real, this->_dual1 + hdn._dual1, this->_dual2 + hdn._dual2, this->_dual12 + hdn._dual12);
}


template <typename T>
HyperDualNumber<T> HyperDualNumber<T>::operator-(const HyperDualNumber &hdn) const {
	return HyperDualNumber(this->_real - hdn._real, this->_dual1 - hdn._dual1, this->_dual2 - hdn._dual2, this->_dual12 - hdn._dual12);
}


template <typename T>
HyperDualNumber<T> HyperDualNumber<T>::operator*(const HyperDualNumber &hdn) const {
	return HyperDualNumber(this->_real * hdn._real, this->_dual1*hdn._real + this->_real*hdn._dual1, this->_dual2*hdn._real + this->_real*hdn._dual2, this->_dual12*hdn._real + this->_real*hdn._dual12 + this->_dual1*hdn._dual2 + this->_dual2*hdn._dual1);
}


template <typename T>
HyperDualNumber<T> HyperDualNumber<T>::operator*(const T &value) const{
	return HyperDualNumber(this->_real * value, this->_dual1 * value, this->_dual2 * value, this->_dual12 * value);
}


template <typename T>
HyperDualNumber<T> HyperDualNumber<T>::operator/(const HyperDualNumber &hdn) const {
    return (*this) * inv(hdn);
} 


template <typename T>
HyperDualNumber<T> HyperDualNumber<T>::operator/(const T &value) const {
	return HyperDualNumber(this->_real / value, this->_dual1 / value, this->_dual2 / value, this->_dual12 / value);
}


template<typename U>
std::ostream& operator<< (std::ostream& stream, const HyperDualNumber<U> &hdn){
	stream << "(" << hdn._real << ", " << hdn._dual1 << ", " << hdn._dual2 << ", " << hdn._dual12 << ")";
    return stream;
}



// ******************************************************************************
template <typename T>
HyperDualNumber<T> HyperDualNumber<T>::inv(const HyperDualNumber<T> & hdn){
	return HyperDualNumber<T>(static_cast<T>(1) / hdn._real,
						 - hdn._dual1 / (hdn._real*hdn._real),
						 - hdn._dual2 / (hdn._real*hdn._real),
						 - (static_cast<T>(2) * hdn._dual1 * hdn._dual2)/(hdn._real*hdn._real*hdn._real) + hdn._dual12 / (hdn._real*hdn._real) );
}

template <typename T>
HyperDualNumber<T> HyperDualNumber<T>::sin(const HyperDualNumber<T> & hdn){
	T value      = std::sin(hdn._real);
	T derivative = std::cos(hdn._real);
	return HyperDualNumber<T>(value, hdn._dual1*derivative, hdn._dual2*derivative, hdn._dual12*derivative - hdn._dual1*hdn._dual2*value);
}

template <typename T>
HyperDualNumber<T> HyperDualNumber<T>::cos(const HyperDualNumber<T> & hdn){
	T value      =  std::cos(hdn._real);
	T derivative = -std::sin(hdn._real);
	return HyperDualNumber<T>(value, hdn._dual1*derivative, hdn._dual2*derivative, hdn._dual12*derivative - hdn._dual1*hdn._dual2*value);
}

template <typename T>
HyperDualNumber<T> HyperDualNumber<T>::tan(const HyperDualNumber<T> & hdn){
	T value      =  std::tan(hdn._real);
	T derivative =  value*value + static_cast<T>(1);
	return HyperDualNumber<T>(value, hdn._dual1*derivative, hdn._dual2*derivative, hdn._dual12*derivative + hdn._dual1*hdn._dual2*static_cast<T>(2)*value*derivative);
}

template <typename T>
HyperDualNumber<T> HyperDualNumber<T>::exp(const HyperDualNumber<T> & hdn){
	T exp_x = std::exp(hdn._real);
	return HyperDualNumber<T>(exp_x, hdn._dual1*exp_x, hdn._dual2*exp_x, (hdn._dual12 + hdn._dual1*hdn._dual2) * exp_x );
}

template <typename T>
HyperDualNumber<T> HyperDualNumber<T>::log(const HyperDualNumber<T> & hdn){
	return HyperDualNumber<T>(std::log(hdn._real), hdn._dual1/hdn._real, hdn._dual2/hdn._real, hdn._dual12/hdn._real - (hdn._dual1/hdn._real)*(hdn._dual2/hdn._real) );
}

template <typename T>
HyperDualNumber<T> HyperDualNumber<T>::abs(const HyperDualNumber<T> & hdn){
	if(hdn._real < static_cast<T>(0)) return -hdn;
	return hdn;
}

template <typename T>
HyperDualNumber<T> HyperDualNumber<T>::pow(const HyperDualNumber<T> & hdn, const T &n){
	// optional step for numerical robustness
	T clampedValue = hdn._real;
	if(std::abs(clampedValue)<std::numeric_limits<T>::epsilon()) 
		clampedValue = clampedValue < static_cast<T>(0) ? -std::numeric_limits<T>::epsilon() : std::numeric_limits<T>::epsilon();
	T derivative = n * std::pow(clampedValue,n-static_cast<T>(1)); // or use hdn._real instead of clampedValue if you don't care numeical robustness

	return HyperDualNumber<T>(std::pow(hdn._real,n), derivative*hdn._dual1, derivative*hdn._dual2, hdn._dual12*derivative + n*(n-1)*hdn._dual1*hdn._dual2*std::pow(clampedValue,n-static_cast<T>(2)));
}

template <typename T>
HyperDualNumber<T> HyperDualNumber<T>::sqrt(const HyperDualNumber<T> & hdn){
	return HyperDualNumber<T>(std::sqrt(hdn._real), static_cast<T>(0.5) * hdn._dual / std::sqrt(hdn._real));
}

