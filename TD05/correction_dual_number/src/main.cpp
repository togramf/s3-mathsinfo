#include <iostream>
#include <string>
#include <iomanip>
#include <functional>
#include <random>
#include <chrono>

#include "DualNumber.hpp"
#include "HyperDualNumber.hpp"




///////////////////////////////////////////////////////////////////////////////////////
// standard numerical central derivalitve for dual numbers
template <typename T>
T centralDerivative(std::function<DualNumber<double>(const DualNumber<double>&)> f, const DualNumber<T> &x, const T &dx){
    return ((f(x+dx)-f(x-dx)) / (dx*static_cast<T>(2))).real();
}



///////////////////////////////////////////////////////////////////////////////////////
void testDualNumers()
{
    const unsigned int precision = 50;

    // function to evaluate using dual numbers
    std::function<DualNumber<double>(const DualNumber<double>&)> f;

    // its derivative formula, to compare our result with the good result
    std::function<double(const double&)> df;

    // easy test
    // f  = [](const DualNumber<double>&x){return x*x;}; // f(x) = x*x
    // df = [](const double&x){return 2*x;}; // f'(x) = 2x

    // f(x) = 5 / (x*x)
    // f  = [](const DualNumber<double>&x){return 5 / (x*x);};
    // df = [](const double &x){return -10 / (x*x*x);};


    // f(x) =  
    // f  = [](const DualNumber<double>&x){return 5 * x  / (x*x + x);};
    // df = [](const double&x){return - 5 / ((x+1)*(x+1));};

    // f  = [](const DualNumber<double>&x){return (x+1)/(x+2);};
    // df = [](const double&x){return  1/( (x+2)*(x+2));};


    // // f(x) = log(2|x|) + exp(x) + sin(x^3)
    f  = [](const DualNumber<double>&x){return DualNumber<double>::log(DualNumber<double>::abs(x)*2) + DualNumber<double>::exp(x) +  DualNumber<double>::sin(DualNumber<double>::pow(x,3));};
    df = [](const double&x){return 1.0/x + std::exp(x) + (std::pow(x,2)*3.0)*std::cos(std::pow(x,3));};

    // f(x) = 2 \sqrt(x) + tan(x)
    // f  = [](const DualNumber<double>&x){return 2.0*DualNumber<double>::sqrt(DualNumber<double>::abs(x)) + DualNumber<double>::tan(DualNumber<double>::abs(x));};
    // df = [](const double&x){ return (std::abs(x)/(pow(cos(x),2))  + std::sqrt(std::abs(x))) / x;};

    // f  = [](const DualNumber<double>&x){return DualNumber<double>::pow(x,3);};
    // df = [](const double&x){return std::pow(x,2)*3.0;};

    // f(x) = tan(x)
    // f  = [](const DualNumber<double>&x){return DualNumber<double>::tan(x);};
    // df = [](const double&x){return 1.0/std::pow(std::cos(x),2);};

    // f(x) = 1/x
    // f  = [](const DualNumber<double>&x){return 1.0/x;};
    // df = [](const double&x){return -1.0/std::pow(x,2);};

    // f(x) = x^(-1.2)
    // f  = [](const DualNumber<double>&x){return DualNumber<double>::pow(x,-1.2);};
    // df = [](const double&x){return -6 / (5* std::pow(x,11/5.0));};


    // select seed from time and some random distribution
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::cout << "seed : " << seed << std::endl;
    std::default_random_engine gen(seed);
    std::uniform_real_distribution<double> uniformRealDistribution(-20,20);
    DualNumber<double> x(0,1); // f(x) = x  => sa dérivée est 1, et x prendra des valeurs dans la boucle for

    // make some tests
    for(unsigned int i=0; i< 10; ++i){
        // select a value for x over the selected distribution
        x.real() = uniformRealDistribution(gen);
        // f(x) via dual numbers
        std::cout << std::setprecision(5) << "function : f (" << x.real() << ") = " << std::setprecision(precision) << f(x).real() << std::endl;
        // f'(x) via computer algebra or derivative function
        std::cout << std::setprecision(5) << "comp alg : f'(" << x.real() << ") = " << std::setprecision(precision) << df(x.real()) << std::endl;
        // f'(x) via dual numbers
        std::cout << std::setprecision(5) << "dual nb  : f'(" << x.real() << ") = " << std::setprecision(precision) << f(x).dual() << std::endl;
        // f'(x) via regular numerical differentiation
        std::cout << std::setprecision(5) << "numerical: f'(" << x.real() << ") = " << std::setprecision(precision) << centralDerivative<double>(f,x,1.0e-10) << std::endl;
        std::cout << std::endl;
    }
}


///////////////////////////////////////////////////////////////////////////////////////
void testHyperDualNumbers()
{
    const unsigned int precision = 50;

     // function to evaluate and its derivative
    std::function<HyperDualNumber<double>(const HyperDualNumber<double>&)> f,df,df2;

    // f(x)   = log(2|x|) + exp(x) + sin(x^3)
    // f'(x)  = 1/X + exp(x) + 3x^2 +cos(x^3)
    // f''(x) = -1/x^2  + exp(x) + 6x cos(x^3) -9x^4sin(x^3)
    f   = [](const HyperDualNumber<double>&x){return HyperDualNumber<double>::log(HyperDualNumber<double>::abs(x)*2) + HyperDualNumber<double>::exp(x) +  HyperDualNumber<double>::sin(HyperDualNumber<double>::pow(x,3));};
    df  = [](const HyperDualNumber<double>&x){return 1.0/x + HyperDualNumber<double>::exp(x) + (HyperDualNumber<double>::pow(x,2)*3.0)*HyperDualNumber<double>::cos(HyperDualNumber<double>::pow(x,3));};
    df2 = [](const HyperDualNumber<double>&x){return -1.0/(x*x) + HyperDualNumber<double>::exp(x) + x*6*HyperDualNumber<double>::cos(HyperDualNumber<double>::pow(x,3)) -9*HyperDualNumber<double>::pow(x,4)*HyperDualNumber<double>::sin(HyperDualNumber<double>::pow(x,3));};

    // f(x)   = x^3 + tan x
    // f'(x)  = 3x^2 + 1/cos^2(x)
    // f''(x) = 6x + 2 tan(x) / cos^2(x)
    // f   = [](const HyperDualNumber<double>&x){return HyperDualNumber<double>::pow(x,3) + HyperDualNumber<double>::tan(x);};
    // df  = [](const HyperDualNumber<double>&x){return HyperDualNumber<double>::pow(x,2)*3.0 + 1.0/(HyperDualNumber<double>::cos(x)*HyperDualNumber<double>::cos(x));};
    // df2 = [](const HyperDualNumber<double>&x){return 6.0*x + 2.0*HyperDualNumber<double>::tan(x)/(HyperDualNumber<double>::cos(x)*HyperDualNumber<double>::cos(x));};

    // f(x)   = x^(-1.2)
    // f'(x)  = -1.2 x^(-2.2)
    // f''(x) = 2.64 x^(-3.2)
    // f   = [](const HyperDualNumber<double>&x){return HyperDualNumber<double>::pow(x,-1.2);};
    // df  = [](const HyperDualNumber<double>&x){return -1.2 * HyperDualNumber<double>::pow(x,-2.2);};
    // df2 = [](const HyperDualNumber<double>&x){return 2.64 * HyperDualNumber<double>::pow(x,-3.2);};

    // select seed from time and some random distribution
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::cout << "seed : " << seed << std::endl;
    std::default_random_engine gen(seed);
    std::uniform_real_distribution<double> uniformRealDistribution(-20,20);
    HyperDualNumber<double> x(0,1,1,0);

    // make some tests
    for(unsigned int i=0; i< 10; ++i){
        // select a value for x over the selected distribution
        x.real() = uniformRealDistribution(gen);
        // f(x) via hyper dual numbers
        std::cout << std::setprecision(5) << "function : f  (" << x.real() << ") = " << std::setprecision(precision) << f(x).real() << std::endl;
        // f'(x) via computer algebra or derivative function
        std::cout << std::setprecision(5) << "comp alg : f' (" << x.real() << ") = " << std::setprecision(precision) << df(x).real() << std::endl;
        // f'(x) via huyper dual numbers
        std::cout << std::setprecision(5) << "dual nb  : f' (" << x.real() << ") = " << std::setprecision(precision) << f(x).dual1() << std::endl;
        // f''(x) via computer algebra or derivative function
        std::cout << std::setprecision(5) << "comp alg : f''(" << x.real() << ") = " << std::setprecision(precision) << df2(x).real() << std::endl;
        // f'(x) via huyper dual numbers
        std::cout << std::setprecision(5) << "dual nb  : f''(" << x.real() << ") = " << std::setprecision(precision) << f(x).dual12() << std::endl;
        std::cout << std::endl;
    }
}


///////////////////////////////////////////////////////////////////////////////////////
int main(){

    // dual numerbers
    testDualNumers();

    // hyper dual numbers
    //testHyperDualNumbers();

    return 0;
}
