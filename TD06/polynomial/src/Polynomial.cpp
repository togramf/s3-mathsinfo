#include "Polynomial.hpp"

#include <fstream>
#include <initializer_list>

#include <Eigen/Dense>


Polynomial::Polynomial(const std::initializer_list<double> &list)
	: _coef(Eigen::VectorXd::Zero(list.size()))
{
/*		size_t i=0;
		for(auto const &elem : list)
			_coef[++i] = elem;*/
	std::copy(list.begin(),list.end(), _coef.data());
}


double Polynomial::operator()(const double &x) const{
	double result = _coef[0];
	for (long double i=1; i<_coef.size(); i++) {
		result += _coef[i]*pow(x,i);
	}
	return result;
}


Polynomial Polynomial::polynomialFromRoot(const Eigen::VectorXd &roots){
	if (roots.size() == 1) {
		return Polynomial({-roots[0],1});
	}
	Polynomial p1({-roots[0],1});
	Polynomial p2({-roots[1],1});
	p1 = p1*p2;
	for (long int i = 2; i<roots.size(); i++){
		p2.a(0) = -roots[i];
		p1 = p1*p2;
	}
	return p1;
}


Eigen::VectorXd Polynomial::findRoots(const unsigned int nbIter) const{
	//calcul poly unitaire 
	Eigen::VectorXd pu(_coef/this->a(this->degree()));

	//création matrice compagnon 
	Eigen::MatrixXd C = Eigen::MatrixXd::Zero(this->degree(), this->degree());
	C.bottomLeftCorner(C.rows()-1, C.cols()-1).setIdentity();
	C.rightCols(1)= -pu.head(this->degree());

	//itération - recherche racines 
	for (uint i = 0; i<nbIter; i++){
		Eigen::HouseholderQR<Eigen::MatrixXd> qr(C);
		Eigen::MatrixXd Q = qr.householderQ();
		Eigen::MatrixXd R = qr.matrixQR().triangularView<Eigen::Upper>();
		C = R*Q;
	}

  	return Eigen::VectorXd(C.diagonal());
}


Polynomial Polynomial::derivative() const{
	Eigen::VectorXd stock(this->degree());
	for(long double i=0;i<this->degree();i++){
		stock[i]=_coef[i+1]*(i+1);                         
	}
	Polynomial result(stock);
	return result;
}


Eigen::VectorXd Polynomial::refineRoots(const Eigen::VectorXd &inputRoots, const int nbIter) const {
	Eigen::VectorXd outputRoots = inputRoots;
	Polynomial derivee = this->derivative();
	for (long double index =0 ; index < outputRoots.size() ; index++ ){
		for (int i = 0; i<nbIter; i++){
			outputRoots[index] = outputRoots[index] -((*this)(outputRoots[index]) / derivee(outputRoots[index]));
		}
	}
  	return outputRoots;
}

// product between 2 polynomials
Polynomial Polynomial::operator*(const Polynomial &p) const {

	Polynomial res(this->degree()+1+p.degree());

    for(size_t i=0; i<size_t(_coef.size()); ++i) {
        for(size_t j=0; j<size_t(p._coef.size()); ++j) {
            res._coef(i + j) += _coef(i) * p._coef(j);
        }
    }

    return res;
}


std::ostream& operator<< (std::ostream& stream, const Polynomial &p){

	bool first_element = true;
	for(int i=p.degree(); i>=0; --i){
		if(std::abs(p.a(i)) > 1.0e-10){
			// sign
			if(p.a(i) < 0) stream << " - ";
			else if(!first_element) stream <<  " + ";
			first_element = false;  // don't print fist sign if it is '+'
			// coef
			if(((std::abs(p.a(i))-1.0) > 1.0e-10) || (i==0)) stream << std::abs(p.a(i));  // don't print coefficients '1' unless it is a scalar
			// power
			switch(i){
				case 0 : break;
				case 1 : stream << "x"; break;
				default : stream << "x^" << i; break;
			}
		}
	}
    return stream;
}