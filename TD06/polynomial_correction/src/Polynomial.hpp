#pragma once

#include <iostream>
#include <fstream>

#include <Eigen/Dense>


/// \brief very simple polynompial class
class Polynomial {

private :
	Eigen::VectorXd _coef;

public :

	// constructor with the degree of the polynomila
	Polynomial(const size_t size) : _coef(Eigen::VectorXd::Zero(size)) {}

	// other constructors and destructor
	Polynomial(const Eigen::VectorXd &coef) : _coef(coef) {}
	Polynomial()  = default;
	~Polynomial() = default;

	// getters, setters
	inline int degree() const {return _coef.size();}

	// polynomiula coefficient setter / getter
	inline double &a(const size_t &index) {return _coef(index);}
	inline const double &a(const size_t &index) const {return _coef(index);}

	// define an empty polynomila by its coefficients
	inline void push_front() {}  
	template <typename... Types> void push_front(double var1, Types... var2);

	// evaluate the polynomial at value 'x'
	double operator()(const double &x) const;

	// arithmetic operators over polynomials
	Polynomial operator+(const Polynomial &p) const {return Polynomial(_coef + p._coef);}
	Polynomial operator-(const Polynomial &p) const {return Polynomial(_coef - p._coef);}
	Polynomial operator-() const {return Polynomial(-_coef);}
	Polynomial operator*(const double &value) const {return Polynomial(_coef*value);}
	Polynomial operator*(const Polynomial &p) const;
	friend Polynomial operator*(const double &value, const Polynomial &p) {return Polynomial(p._coef*value);}

	// polynomial tools
	Polynomial derivative() const;
	static Polynomial polynomialFromRoot(const Eigen::VectorXd &roots);
	Eigen::VectorXd findRoots(const unsigned int nbIter = 10) const;
	Eigen::VectorXd refineRoots(const Eigen::VectorXd &roots, const int nbIter) const;

	// print
	friend std::ostream& operator<< (std::ostream& stream, const Polynomial &p);

};


template <typename... Types> 
void Polynomial::push_front(double var1, Types... var2) 
{ 
	Eigen::VectorXd tmp(_coef.size()+1);
	tmp(0) = var1;
	tmp.tail(_coef.size()) = _coef;
	_coef = Eigen::VectorXd(tmp);
  
    push_front(var2...) ;
} 

