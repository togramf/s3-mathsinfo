#include "Polynomial.hpp"

#include <fstream>
#include <Eigen/Dense>


double Polynomial::operator()(const double &x) const{
	double res = _coef(_coef.size() -1);

	for (int i = _coef.size() - 2; i >= 0 ; --i)
		res= (res * x) + _coef(i);

  return res;
}


Polynomial Polynomial::polynomialFromRoot(const Eigen::VectorXd &roots){

	Polynomial res;
	res.push_front(1);

	for(int i=0; i<roots.size(); ++i){
		Polynomial monomial;
		monomial.push_front(1,-roots(i));
		res = res * monomial;
	}

	return res;
}


Eigen::VectorXd Polynomial::findRoots(const unsigned int nbIter) const{

  // build the compagnon matrix
  Eigen::VectorXd unitPoly(_coef / (_coef(_coef.size()-1)));
  Eigen::MatrixXd C = Eigen::MatrixXd::Zero(_coef.size()-1, _coef.size()-1);
  C.bottomLeftCorner(C.rows()-1, C.cols()-1).setIdentity();
  C.rightCols(1) = -unitPoly.head(unitPoly.size()-1);

  // iterative solver
  for (int i = 0; i < nbIter; ++i){
    Eigen::HouseholderQR<Eigen::MatrixXd> qr(C);
    Eigen::MatrixXd Q = qr.householderQ();
    Eigen::MatrixXd R = qr.matrixQR().triangularView<Eigen::Upper>();
    C = R * Q;
  }

  // find roots
  return C.diagonal();
}


Polynomial Polynomial::derivative() const{
	Polynomial p(_coef.size()-1);
  	for(int i=0; i<p.degree(); ++i)
    	p.a(i) = _coef(i+1)*(i+1);

	return p;
}


Eigen::VectorXd Polynomial::refineRoots(const Eigen::VectorXd &inputRoots, const int nbIter) const {

  // derivative of a polynomial
  Polynomial deriv = this->derivative();

  // non linear optimization for each root
  Eigen::VectorXd roots(inputRoots);
  for(int r=0; r<roots.size(); ++r)
  	for(int i=0; i<nbIter; ++i)
    	roots(r) = roots(r) - (*this)(roots(r)) / deriv(roots(r));

    return roots;
}



// product between 2 polynomials
Polynomial Polynomial::operator*(const Polynomial &p) const {

	Polynomial res(this->degree()+p.degree()-1);

    for(size_t i=0; i<_coef.size(); ++i) {
        for(size_t j=0; j<p._coef.size(); ++j) {
            res._coef(i + j) += _coef(i) * p._coef(j);
        }
    }

    return res;
}


std::ostream& operator<< (std::ostream& stream, const Polynomial &p){

	bool first_element = true;
	for(int i=p.degree()-1; i>=0; --i){
		if(std::abs(p.a(i)) > 1.0e-10){
			// sign
			if(p.a(i) < 0) stream << " - ";
			else if(!first_element) stream <<  " + ";
			first_element = false;  // don't print fist sign if it is '+'
			// coef
			if(((std::abs(p.a(i))-1.0) > 1.0e-10) || (i==0)) stream << std::abs(p.a(i));  // don't print coefficients '1' unless it is a scalar
			// power
			switch(i){
				case 0 : break;
				case 1 : stream << "x"; break;
				default : stream << "x^" << i; break;
			}
		}
	}
    return stream;
}