#include <iostream>
#include <iomanip> 

// Exo5 : Racine carrée

// sur Linus : 
//  compilation : g++ exo5.cpp -Wall -O2 -o exo5
//  exécution   : ./exo5


double mySqrt(double a);
double newtonSqrt(double a);

int main()
{      
    double a=0.2;
    std::cout << "Ma racine : " << mySqrt(a) << std::endl;
    std::cout << "Pas ma racine : " << newtonSqrt(a) << std::endl;

    return 0;
}

double mySqrt(double a){
    double x=1.0;
    if(a==1.0) 
        return x;
    else if (a>1.0){
        while(x*x<=a){
            x++;
        }
        x=x-1;
        while(x*x<=a){
            x+=0.1;
        }
        x=x-0.1;
        while(x*x<=a){
            x+=0.01;
        }
        return x-0.01;
    }else{
        while(x*x>=a){
            x-=0.1;
        }
        x=x+0.1;
        while(x*x>=a){
            x-=0.01;
        }
        return x;
    }
        
}

double newtonSqrt(double a){
    double u0=1;
    double stock=u0;
    double un;
        for(int i=0;i<5;i++){
            un=(stock+a/stock)*0.5;
            stock=un;
        }
    return un;
}