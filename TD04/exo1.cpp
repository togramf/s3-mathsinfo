#include <iostream>
#include <Eigen/Dense>
#include <ctime>

// Exo1 : Gauss-Seidel

// --> PAS FINI : reprendre ligne 68

/** NOTICE 
 * compilation : g++ -Wall -O2 -I /usr/include/eigen3 exo1.cpp -o exo1
 * execution   : ./exo1.out 
 */

Eigen::MatrixXd MatrixDiagDom(size_t size){
    
    Eigen::MatrixXd M = Eigen::MatrixXd::Zero(size,size);
    Eigen::MatrixXd Id = Eigen::MatrixXd::Identity(size,size);

    int alpha = size;

    for(size_t i = 0 ; i < size ; i++){
        for (size_t j = 0; j < size; j++)
        {
            M(i,j)=std::rand()%2-1;
        }
        
    }

    return M+alpha*Id;

}

Eigen::VectorXd GaussSeidel(const Eigen::MatrixXd &A, const Eigen::VectorXd &b, const uint nbIter){

    Eigen::VectorXd result = b;

    for (uint n=0; n<nbIter ; n++)
        for (long int i = 0 ; i<result.size(); i++)
            result[i]=(b[i]-A.row(i)*result+A(i,i)*result[i])/A(i,i);
    
    return result;
}

Eigen::VectorXd GaussSeidelDecompo(const Eigen::MatrixXd &A, const Eigen::VectorXd &b, const uint nbIter){

    Eigen::VectorXd result = b;
    Eigen::MatrixXd D = Eigen::MatrixXd::Zero(A.rows(),A.cols()) ; //diagonale de A
    Eigen::MatrixXd E = Eigen::MatrixXd::Zero(A.rows(),A.cols()) ; //triangle sup de A
    Eigen::MatrixXd F = Eigen::MatrixXd::Zero(A.rows(),A.cols()) ; //triangle inf de A
    
    for (long int i=0; i<A.cols(); i++){
        for (long int j=0; j<A.rows(); j++){
            if(i<j){
                D(i,j)=0;
                F(i,j)=0;
            } else if (i == j){
                E(i,j)=0;
                F(i,j)=0;
            } else {
                D(i,j)=0;
                E(i,j)=0;
            }
        }
    }

    for (uint n=0; n<nbIter ; n++)
        for (long int i = 0 ; i<result.size(); i++){
            Eigen::Vector3d res1 = (D+E).inverse()*F*result;
            Eigen::Vector3d res2 = (D+E).inverse()*b;
            result[i] =  -res1.sum(res2); //REPRENDRE ICI 
        }   
    return result;
}

int main(){

    size_t size=3;
    Eigen::MatrixXd A = MatrixDiagDom(size);
    Eigen::VectorXd b = Eigen::Vector3d(10,5,-7);
    
     
    A.row(0)=Eigen::Vector3d(5,-3,1);
    A.row(1)=Eigen::Vector3d(-1,6,-4);
    A.row(2)=Eigen::Vector3d(3,-3,9);

    Eigen::MatrixXd Gauss = GaussSeidel(A,b,8);
    
    std::cout << "resultat normal " << Gauss << std::endl;
    std::cout << "resultat decompo " << GaussSeidelDecompo(A,b,8) << std::endl;
    // std::cout << "reste pour n=5  " << (A*GaussSeidel(A,b,5)-b).norm() << std::endl;
    // std::cout << "reste pour n=8  " << (A*GaussSeidel(A,b,8)-b).norm() << std::endl;
    // std::cout << "reste normal pour n=10 " << (A*GaussSeidel(A,b,10)-b).norm() << std::endl;

    return 0;
}