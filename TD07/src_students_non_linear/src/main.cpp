#include <iostream>
#include <iomanip>
#include <functional>
#include <cmath>
#include "DualNumber.hpp"


double dichotomy(std::function<double(const double&)> &f, 
                  double lowerBound, 
                  double upperBound, 
                  const unsigned int nbIterations){

    for(uint i=0;i<nbIterations;i++){
        double middle=(lowerBound+upperBound)/2;
        if(f(lowerBound)*f(middle)<0)
            upperBound=middle;
        else
            lowerBound=middle;
    }

    return (lowerBound + upperBound)*0.5;
}

unsigned int dichotomyNbIteration(const double &lowerBound, const double &upperBound, const double &precision){

    return log2((upperBound-lowerBound)/precision)-1;

}


double Newton(std::function<double(const double&)> &f, 
              std::function<double(const double&)> &derivative,
              const double &input,
              const double &threshold,
              int &maxIterations) // ref just to get back the number of iterations
{
    double x=input;
    while(fabs(f(x))>threshold){
        x=x-f(x)/derivative(x);
        maxIterations++;
    }

    return x;
}

double NewtonDual (std::function<DualNumber(const double&)> &f, 
              std::function<DualNumber(const double&)> &derivative, 
              const double &input,
              const double &threshold,
              int &maxIterations) // ref just to get back the number of iterations
{
    double x=input;
    while(fabs(f(x))>threshold){
        x=x-f(x)/derivative(x);
        maxIterations++;
    }

    return x;
}


void mainDichotomy()
{
    std::function<double(const double&)> f = [](const double&x){return cos(x) - 2*x;};
    const double precision = 1.0e-7;
    const unsigned int nbIterations = dichotomyNbIteration(-M_PI,M_PI,precision);

    std::cout << "DICHOTOMIE pour la fonction f2 "<< std::endl;
    std::cout << "requested precision  : " << precision << std::endl;
    std::cout << "suggested iterations : " << nbIterations << std::endl;
    double root = dichotomy(f,-M_PI,M_PI,nbIterations);
    std::cout << "dichotomy            : " << root << std::endl;
    std::cout << "f(root)              : " << f(root) << std::endl << std::endl;
}

void mainNewton()
{
    //fonction 1, x1
    std::function<double(const double&)> f, derivative;
    f = [](const double&x){return ((exp(x)-1.0)/(exp(x)+1.0)) + (3.0/4.0) ;};
    derivative = [](const double&x){return 2.0*exp(x) / ((exp(x)+1.0)*(exp(x)+1.0)) ;};

    DualNumber <double> fonction(f,derivative);

    const double precision = 1.0e-7;
    int maxIterations = 0;
    const double x = 0;

    std::cout << "NEWTON pour la fonction f1 "<< std::endl;
    std::cout << "requested precision  : " << precision << std::endl;
    std::cout << "input estimation    : " << x << std::endl;
    double root = Newton(fonction.real(),fonction.dual(),x,precision,maxIterations);
    std::cout << "Newton               : " << root << std::endl;
    std::cout << "f(root)              : " << f(root) << std::endl;
    std::cout << "nb iterations        : " << maxIterations << std::endl << std::endl;

    //fonction 1, x2
    maxIterations = 0;
    const double x2 = 2;

    std::cout << "NEWTON pour la fonction f1 " << std::endl;
    std::cout << "requested precision  : " << precision << std::endl;
    std::cout << "input estimation    : " << x2 << std::endl;
    root = Newton(f,derivative,x2,precision,maxIterations);
    std::cout << "Newton               : " << root << std::endl;
    std::cout << "f(root)              : " << f(root) << std::endl;
    std::cout << "nb iterations        : " << maxIterations << std::endl <<std::endl;

    //fonction 2, x1
    f = [](const double&x){return cos(x) - 2*x;};
    derivative = [](const double&x){return -sin(x) - 2;};

    maxIterations = 0;

    std::cout << "NEWTON pour la fonction  f2 " << std::endl;
    std::cout << "requested precision  : " << precision << std::endl;
    std::cout << "input estimation    : " << x << std::endl;
    root = Newton(f,derivative,x,precision,maxIterations);
    std::cout << "Newton               : " << root << std::endl;
    std::cout << "f(root)              : " << f(root) << std::endl;
    std::cout << "nb iterations        : " << maxIterations << std::endl << std::endl;

}


int main()
{
    mainDichotomy();

    mainNewton();

    return 0;
}

