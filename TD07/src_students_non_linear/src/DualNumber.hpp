#pragma once

#include <fstream>
#include <cmath>

template <typename T>
class DualNumber{

	private : 
		T _real;
		T _dual;

	public :

		// default constructor
		DualNumber() = default;

		// constructor by value
		DualNumber(const T& real, const T &dual) : _real(real), _dual(dual) {};

		// convert a scalar into a dual number		
		DualNumber(const T &value) : _real(value), _dual(static_cast<T>(0)) {}; 

		// copy constructor
		DualNumber(const DualNumber &dn) = default;

		// destructor
		~DualNumber() = default;

		// getter / setter
		inline T & real() {return _real;};
		inline T & dual() {return _dual;};
		inline const T & real() const {return _real;};
		inline const T & dual() const {return _dual;};


		// some operators
		DualNumber operator*(const DualNumber &dn) const;
		DualNumber operator+(const DualNumber &dn) const;
		DualNumber operator-(const DualNumber &dn) const;
		DualNumber operator/(const DualNumber &dn) const;
		inline DualNumber operator/(const T &value) const {return DualNumber(_real / value, _dual/value);}
		inline friend DualNumber operator*(const T &value, const DualNumber &dn) {return dn * value;}
		inline friend DualNumber operator/(const T &value, const DualNumber &dn) {return DualNumber(value)/dn;};
		inline friend DualNumber operator-(const DualNumber &dn) {return DualNumber(-dn._real,-dn._dual);}; // unary minus


		static DualNumber sin(const DualNumber & dn);	
		static DualNumber cos(const DualNumber & dn);
		static DualNumber tan(const DualNumber & dn);
		static DualNumber exp(const DualNumber & dn);
		static DualNumber log(const DualNumber & dn);
		static DualNumber abs(const DualNumber & dn);
		static DualNumber pow(const DualNumber & dn, const T &n);
		static DualNumber sqrt(const DualNumber & dn);

		template<typename U>
        friend std::ostream& operator<< (std::ostream& stream, const DualNumber<U> &dn);
};


template<typename T>
std::ostream& operator<< (std::ostream& stream, const DualNumber<T> &dn){
	stream << "(" << dn._real << ", " << dn._dual << ")";
    return stream;
}


template <typename T>
DualNumber<T> DualNumber<T>::operator*(const DualNumber<T> &dn) const {
	return DualNumber<T>(this->_real * dn._real, this->_dual*dn._real + this->_real*dn._dual);
}

template <typename T>
DualNumber<T> DualNumber<T>::operator+(const DualNumber<T> &dn) const {
	return DualNumber<T>(this->_real + dn._real, this->_dual + dn._dual);
}

template <typename T>
DualNumber<T> DualNumber<T>::operator-(const DualNumber<T> &dn) const {
	return DualNumber<T>(this->_real - dn._real, this->_dual - dn._dual);
}

template <typename T>
DualNumber<T> DualNumber<T>::operator/(const DualNumber<T> &dn) const {
	return DualNumber<T>(this->_real / dn._real, (this->_dual*dn._real - this->_real*dn.dual)/(dn.real*dn.real));
}


template <typename T>
DualNumber<T> DualNumber<T>::sin(const DualNumber<T> & dn){
	return DualNumber<T>(std::sin(dn._real),dn._dual*std::cos(dn._real));
}

template <typename T>
DualNumber<T> DualNumber<T>::cos(const DualNumber<T> & dn){
	return DualNumber<T>(std::cos(dn._real),dn._dual*std::sin(dn._real));
}

template <typename T>
DualNumber<T> DualNumber<T>::tan(const DualNumber<T> & dn){
	return DualNumber<T>::sin(dn)/DualNumber<T>::cos(dn);
}

template <typename T>
DualNumber<T> DualNumber<T>::exp(const DualNumber<T> & dn){
	return DualNumber<T>(std::exp(dn._real),dn._dual*std::exp(dn._real));
}

template <typename T>
DualNumber<T> DualNumber<T>::log(const DualNumber<T> & dn){
	return DualNumber<T>(std::log(dn._real),dn._dual/dn._real);
}

template <typename T>
DualNumber<T> DualNumber<T>::abs(const DualNumber<T> & dn){
	return DualNumber<T>(std::abs(dn._real),dn._dual*(dn._real/std::abs(dn._real)));
}

template <typename T>
DualNumber<T> DualNumber<T>::pow(const DualNumber<T> & dn, const T &n){
	return DualNumber<T>(std::pow(dn._real,n),n*std::pow(dn._real,n-1)*dn._dual);
}

template <typename T>
DualNumber<T> DualNumber<T>::sqrt(const DualNumber<T> & dn){
	return DualNumber<T>(std::sqrt(dn._real,dn._dual/2*std::sqrt(dn._real)));
}