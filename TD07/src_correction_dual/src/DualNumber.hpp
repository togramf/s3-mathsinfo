#pragma once

#include <fstream>
#include <cmath>

// some doc here: from https://en.wikipedia.org/wiki/Automatic_differentiation#:~:text=Automatic%20differentiation%20using%20dual%20numbers,-Forward%20mode%20automatic&text=An%20additional%20component%20is%20added,the%20algebra%20of%20dual%20numbers.

template <typename T>
class DualNumber{

	private : 
		T _real;
		T _dual;

	public :
		// default constructor
		DualNumber() = default;

		// constructor by value
		DualNumber(const T& real, const T &dual) : _real(real), _dual(dual) {};

		// convert a scalar into a dual number		
		DualNumber(const T &value) : _real(value), _dual(static_cast<T>(0)) {}; 

		// copy constructor
		DualNumber(const DualNumber &dn) = default;

		// destructor
		~DualNumber() = default;

		// getter / setter
		inline T & real() {return _real;};
		inline T & dual() {return _dual;};
		inline const T & real() const {return _real;};
		inline const T & dual() const {return _dual;};

		// some operators
		DualNumber operator+(const DualNumber &dn) const;
		DualNumber operator-(const DualNumber &dn) const;
		DualNumber operator*(const DualNumber &dn) const;
		DualNumber operator/(const DualNumber &dn) const;
		inline friend DualNumber operator*(const T &value, const DualNumber &dn) {return dn*value;};
		inline friend DualNumber operator/(const T &value, const DualNumber &dn) {return DualNumber(value)/dn;};
		inline friend DualNumber operator-(const DualNumber &dn) {return DualNumber(-dn._real,-dn._dual);}; // unary minus


		inline DualNumber conjugate() const {return DualNumber(_real, -_dual);};
		inline DualNumber operator~() const {return this->conjugate();};
		inline DualNumber inv() const {return static_cast<T>(1) / (*this);};
		inline T norm2() const {return (*this) * this->conjugate();};
		inline T norm()  const {return sqrt(norm2());};

		static DualNumber sin(const DualNumber & dn);
		static DualNumber cos(const DualNumber & dn);
		static DualNumber tan(const DualNumber & dn);
		static DualNumber exp(const DualNumber & dn);
		static DualNumber log(const DualNumber & dn);
		static DualNumber abs(const DualNumber & dn);
		static DualNumber pow(const DualNumber & dn, const T &n);
		static DualNumber sqrt(const DualNumber & dn);

		template<typename U>
        friend std::ostream& operator<< (std::ostream& stream, const DualNumber<U> &dn);
};

template <typename T>
DualNumber<T> DualNumber<T>::operator+(const DualNumber<T> &dn) const {
	return DualNumber<T>(this->_real + dn._real, this->_dual + dn._dual);
}


template <typename T>
DualNumber<T> DualNumber<T>::operator-(const DualNumber<T> &dn) const {
	return DualNumber<T>(this->_real - dn._real, this->_dual - dn._dual);
}


template <typename T>
DualNumber<T> DualNumber<T>::operator*(const DualNumber<T> &dn) const {
	return DualNumber<T>(this->_real * dn._real, this->_dual*dn._real + this->_real*dn._dual);
}


template <typename T>
DualNumber<T> DualNumber<T>::operator/(const DualNumber<T> &dn) const {
	return DualNumber<T>(this->_real/dn._real, (this->_dual*dn._real - this->_real*dn._dual) / (dn._real * dn._real)); 
} 


template<typename T>
std::ostream& operator<< (std::ostream& stream, const DualNumber<T> &dn){
	stream << "(" << dn._real << ", " << dn._dual << ")";
    return stream;
}


template <typename T>
DualNumber<T> DualNumber<T>::sin(const DualNumber<T> & dn){
	return DualNumber<T>(std::sin(dn._real),dn._dual*std::cos(dn._real));
}

template <typename T>
DualNumber<T> DualNumber<T>::cos(const DualNumber<T> & dn){
	return DualNumber<T>(std::cos(dn._real),-dn._dual*std::sin(dn._real));
}

template <typename T>
DualNumber<T> DualNumber<T>::tan(const DualNumber<T> & dn){
	return DualNumber<T>(std::tan(dn._real),dn._dual/std::pow(std::cos(dn._real),2));
}

template <typename T>
DualNumber<T> DualNumber<T>::exp(const DualNumber<T> & dn){
	return DualNumber<T>(std::exp(dn._real),dn._dual*std::exp(dn._real));
}

template <typename T>
DualNumber<T> DualNumber<T>::log(const DualNumber<T> & dn){
	return DualNumber<T>(std::log(dn._real),dn._dual/dn._real);
}

template <typename T>
DualNumber<T> DualNumber<T>::abs(const DualNumber<T> & dn){
	return DualNumber<T>(std::abs(dn._real), (dn._real<0)?(-dn._dual):dn._dual);
}

template <typename T>
DualNumber<T> DualNumber<T>::pow(const DualNumber<T> & dn, const T &n){
	return DualNumber<T>(std::pow(dn._real,n), n * std::pow(dn._real,n-static_cast<T>(1)) * dn._dual);
}

template <typename T>
DualNumber<T> DualNumber<T>::sqrt(const DualNumber<T> & dn){
	return DualNumber<T>(std::sqrt(dn._real), static_cast<T>(0.5) * dn._dual / std::sqrt(dn._real));
}

