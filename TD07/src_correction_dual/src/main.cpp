#include <iostream>
#include <iomanip>
#include <functional>
#include <cmath>
#include <stdexcept>  

#include "DualNumber.hpp"

///////////////////////////////////////////////////////////
double dichotomy(std::function<double(const double&)> &f, 
                  double lowerBound, // a copy to modify this value in the code
                  double upperBound, // a copy to modify this value in the code
                  const unsigned int nbIterations){

    // check bounds
    if(lowerBound >= upperBound) 
        throw std::invalid_argument("dichotomy: lowerBound<upperBound not respected (" + std::to_string(lowerBound) + "," + std::to_string(upperBound) + ")");

    // dichotomy iterations
    for(int i=0; i<nbIterations; ++i){
        double midle = (lowerBound + upperBound) * 0.5;
        if(f(lowerBound) * f(midle) < 0)
            upperBound = midle;
        else lowerBound = midle;
    }

    // return midle of the final interval
    return (lowerBound + upperBound)*0.5;
}


///////////////////////////////////////////////////////////
// how many dichotomic iterations for a given precision according to the considered interval
unsigned int dichotomyNbIteration(const double &lowerBound, const double &upperBound, const double &precision){

    // check bounds
    if(lowerBound >= upperBound) 
        throw std::invalid_argument("dichotomy: lowerBound<upperBound not respected (" + std::to_string(lowerBound) + "," + std::to_string(upperBound) + ")");

    return ceil(log((upperBound-lowerBound)/precision)/log(2.0) - 1.0);
}


///////////////////////////////////////////////////////////
double Newton(std::function<double(const double&)> &f, 
              std::function<double(const double&)> &derivative, 
              const double &input,
              const double &threshold,
              const int &maxIterations,
              int &nbIterationsToConverge){

    double x = input;

    unsigned int nbIterations = 0;
    while(std::abs(f(x)) > threshold){
        x = x - f(x) / derivative(x);
        nbIterations++;
        if(nbIterations > maxIterations)
            break;
    }

    // just for information
    nbIterationsToConverge = nbIterations; 

    return x;
}


///////////////////////////////////////////////////////////
DualNumber<double> NewtonDual(std::function<DualNumber<double>(const DualNumber<double>&)> &f, 
              const DualNumber<double> &input,
              const double &threshold,
              const int &maxIterations,
              int &nbIterationsToConverge){

    DualNumber<double> x = input;

    unsigned int nbIterations = 0;
    while(std::abs(f(x).real()) > threshold){
        DualNumber<double> value = f(x);
        x = x - ( value.real() / value.dual() );
        nbIterations++;
        if(nbIterations > maxIterations)
            break;
    }

    // just for information
    nbIterationsToConverge = nbIterations; 

    return x;
}


///////////////////////////////////////////////////////////
void mainDichotomy()
{
    std::function<double(const double&)> f = [](const double&x){return cos(x) - 2*x;};
    const double precision = 1.0e-7;
    const unsigned int nbIterations = dichotomyNbIteration(-M_PI,M_PI,precision);

    std::cout << "requested precision  : " << precision << std::endl;
    std::cout << "suggested iterations : " << nbIterations << std::endl;
    double root = dichotomy(f,-M_PI,M_PI,nbIterations);
    std::cout << "dichotomy            : " << root << std::endl;
    std::cout << "f(root)              : " << f(root) << std::endl << std::endl;
}


///////////////////////////////////////////////////////////
void mainNewton()
{
    std::function<double(const double&)> f, derivative;
    // f = [](const double&x){return ((exp(x)-1.0)/(exp(x)+1.0)) + (3.0/4.0) ;};
    // derivative = [](const double&x){return 2.0*exp(x) / ((exp(x)+1.0)*(exp(x)+1.0)) ;};
    f = [](const double&x){return cos(x) - 2*x;};
    derivative = [](const double&x){return -sin(x) - 2;};

    const double precision = 1.0e-7;
    int maxIterations = 20;
    int nbIterationsToConverge = 0;
    const double x = 0.0;

    std::cout << "requested precision  : " << precision << std::endl;
    std::cout << "input estimattion    : " << x << std::endl;
    double root = Newton(f,derivative,x,precision,maxIterations,nbIterationsToConverge);
    std::cout << "Newton               : " << root << std::endl;
    std::cout << "f(root)              : " << f(root) << std::endl;
    std::cout << "nb iterations        : " << nbIterationsToConverge << std::endl << std::endl;
}


///////////////////////////////////////////////////////////
void mainNewtonDual()
{
    std::function<DualNumber<double>(const DualNumber<double>&)> f;
    // f = [](const DualNumber<double>&x){return ((DualNumber<double>::exp(x)-1.0)/(DualNumber<double>::exp(x)+1.0)) + (3.0/4.0) ;};
    f = [](const DualNumber<double>&x){return DualNumber<double>::cos(x) - 2*x;};

    const double precision = 1.0e-7;
    int maxIterations = 20;
    int nbIterationsToConverge = 0;
    const DualNumber<double> x(0.0,1.0);

    std::cout << "requested precision  : " << precision << std::endl;
    std::cout << "input estimattion    : " << x.real() << std::endl;
    DualNumber<double> root = NewtonDual(f,x,precision,maxIterations,nbIterationsToConverge);
    std::cout << "Newton               : " << root.real() << std::endl;
    std::cout << "f(root)              : " << f(root).real() << std::endl;
    std::cout << "nb iterations        : " << nbIterationsToConverge << std::endl << std::endl;
}


///////////////////////////////////////////////////////////
int main()
{
    mainDichotomy();

    mainNewton();

    mainNewtonDual();

    return 0;
}

