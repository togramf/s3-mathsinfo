#include <iostream>
#include <Eigen/Dense>
#include <ctime>

// Exo3 : Produit scalaire

// sur Linus : 
//  compilation :  g++ exo3.cpp -Wall -O2 -I /usr/include/eigen3 -o tp2ex3
//  exécution   : ./tp2ex3

double dotProduct(const Eigen::VectorXd &v1, const Eigen::VectorXd &v2){
    double product=0.0;
    if(v1.size()==v2.size()){
        
        for(int i=0;i<v1.size();i++){
            product+=v1[i]*v2[i];
        }
        

    }else
        std::cerr << "OnO c'est pas la mm taille !" << std::endl;
    
    return product;

}

int main()
{   
    const unsigned int size=100000;
    Eigen::VectorXd v1 = Eigen::VectorXd::Random(size);
    Eigen::VectorXd v2 = Eigen::VectorXd::Random(size);
    std::cout << "le produit scalaire de v1 et v2 vaut : " << dotProduct(v1,v2) << std::endl;
    std::cout << "                               Eigen : " << v1.dot(v2) << std::endl;

    clock_t begin = clock();
    dotProduct(v1,v2);
    clock_t end = clock();
    double tempsCalc = double(end - begin) / CLOCKS_PER_SEC;
    std::cout << "temps calcul du produit scalaire (le nôtre) :   " <<tempsCalc <<"s " << std::endl;

    begin = clock();
    v1.dot(v2);
    end = clock();
    tempsCalc = double(end - begin) / CLOCKS_PER_SEC;
    std::cout << "temps calcul du produit scalaire (Eigen pabo) : " <<tempsCalc <<"s " << std::endl;

    
    return 0;
}

