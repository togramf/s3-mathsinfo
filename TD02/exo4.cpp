#include <iostream>
#include <Eigen/Dense>
#include <ctime>

// Exo4 : Multiplication de 2 matrices

// sur Linus : 
//  compilation :  g++ exo4.cpp -Wall -O2 -I /usr/include/eigen3 -o tp2ex4
//  exécution   : ./tp2ex4

Eigen::MatrixXd matrixProduct(const Eigen::MatrixXd &m1, const Eigen::MatrixXd &m2){
    
    Eigen::MatrixXd product = Eigen::MatrixXd::Zero(m1.rows(),m2.cols());
    if(m1.cols()==m2.rows()){

        for(int i=0;i<m1.rows();i++){
            for(int j=0;j<m2.cols();j++){
                for (int k=0; k<m1.cols(); k++){
                    product(i,j)+= m1(i,k) * m2(k,j);
                }
            }
        }

    }else
        std::cerr << "OnO c'est pas la bonne taille !" << std::endl;
    return product;
}


int main(){
    const unsigned int size  = 2000;
    Eigen::MatrixXd A = Eigen::MatrixXd::Random(size, size);
    Eigen::MatrixXd B = Eigen::MatrixXd::Random(size, size);
    
    // std::cout << "le produit matriciel vaut : " << matrixProduct(A,B) << std::endl;
    // std::cout << "                    Eiden : " << A*B << std::endl; 
    Eigen::MatrixXd C = matrixProduct(A,B);                 //très long
    double error = (C-A*B).norm() / (C.rows()*C.cols());    //très long
    std::cout << error << std::endl;                        //idem

    clock_t begin = clock();
    matrixProduct(A,B);                                     // tout pareil
    clock_t end = clock();
    double tempsCalc = double(end - begin) / CLOCKS_PER_SEC;
    std::cout << "temps calcul du produit matriciel (le nôtre) :   " <<tempsCalc <<"s " << std::endl;

    begin = clock();
    A*B;
    end = clock();
    tempsCalc = double(end - begin) / CLOCKS_PER_SEC;
    std::cout << "temps calcul du produit matriciel (Eigen pabo) : " <<tempsCalc <<"s " << std::endl;
    return 0;
}