#include <iostream>
#include <Eigen/Dense>
#include <ctime>

// Exo5 : Multiplication de Strassen

// sur Linus : 
//  compilation :  g++ exo5.cpp -Wall -O2 -I /usr/include/eigen3 -o tp2ex5
//  exécution   : ./tp2ex5

Eigen::MatrixXd matrixProduct(const Eigen::MatrixXd &m1, const Eigen::MatrixXd &m2){
    
    Eigen::MatrixXd product = Eigen::MatrixXd::Zero(m1.rows(),m2.cols());
    if(m1.cols()==m2.rows()){

        for(int i=0;i<m1.rows();i++){
            for(int j=0;j<m2.cols();j++){
                for (int k=0; k<m1.cols(); k++){
                    product(i,j)+= m1(i,k) * m2(k,j);
                }
            }
        }

    }else
        std::cerr << "OnO c'est pas la bonne taille !" << std::endl;
    return product;
}

Eigen::MatrixXd strassenProduct(const Eigen::MatrixXd &m1, const Eigen::MatrixXd &m2){
    double a,b,c,d,e,f,g,h;
    a = m1(0,0);
    b = m1(0,1);
    c = m1(1,0);
    d = m1(1,1);
    e = m2(0,0);
    f = m2(0,1);
    g = m2(1,0);
    h = m2(1,1);

    double p1,p2,p3,p4,p5,p6,p7;
    p1 = a*(f-h);
    p2=(a+b)*h;
    p3=(c+d)*e;
    p4=d*(g-e);
    p5=(a+d)*(e+h);
    p6=(b-d)*(g+h);
    p7=(a-c)*(e+f);

    Eigen::MatrixXd result = Eigen::MatrixXd::Zero(2,2);  
    result(0,0)=p5+p4-p2+p6;
    result(0,1)=p1+p2;
    result(1,0)=p3+p4;
    result(1,1)=p1+p5-p3-p7;
    // std::cout << "Eiden : " << A*B << std::endl; 

    return result;
}

int main(){
    const unsigned int size  = 2;
    Eigen::MatrixXd A = Eigen::MatrixXd::Random(size, size);
    Eigen::MatrixXd B = Eigen::MatrixXd::Random(size, size);

    std::cout << "Test de performance des produits matriciels : "<<std::endl;
    // std::cout << "Strassen : "<< strassenProduct(A,B) << std::endl;
    // std::cout << "Eiden : " << A*B << std::endl; 

    clock_t begin = clock();
    strassenProduct(A,B);
    clock_t end = clock();
    double tempsCalc = double(end - begin) / CLOCKS_PER_SEC;
    std::cout << "Strassen :   " <<tempsCalc <<"s " << std::endl;

    begin = clock();
    matrixProduct(A,B);
    end = clock();
    tempsCalc = double(end - begin) / CLOCKS_PER_SEC;
    std::cout << "Notre : " <<tempsCalc <<"s " << std::endl;

    begin = clock();
    A*B;
    end = clock();
    tempsCalc = double(end - begin) / CLOCKS_PER_SEC;
    std::cout << "Eigen : " <<tempsCalc <<"s " << std::endl;


    return 0;
}