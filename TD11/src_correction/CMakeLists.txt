cmake_minimum_required(VERSION 3.15)

project(exponential)

# file to comile
add_executable(exponential src/main.cpp)

# compilation flags
target_compile_features(exponential PRIVATE cxx_std_11)
target_compile_options(exponential PRIVATE -Wall -O2 -Wno-unused-variable) 


