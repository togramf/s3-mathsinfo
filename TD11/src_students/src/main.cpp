#include <iostream>
#include <iomanip>
#include <cmath>
#include <random>
#include <chrono>
#include <functional>
#include <string>



template<typename T>
constexpr T factorial(const unsigned int n){
    return (n == 0) ? 1 : static_cast<T>(n) * factorial<T>(n-1);
}

//EXERCICE 2, question 3
// fonction d'estimation autour de 0, plus on s'éloigne et plus l'erreur est grande
template<typename T>
T taylor(const T &x){
    T x_abs = std::abs(x);
    T N = x_abs;
    T y = static_cast<T>(1) + x_abs;
    for (uint n = 2; n<21; n++){
        N *= x_abs;
        y += N/factorial<unsigned long>(n);
    }
    return (x<static_cast<T>(0)) ? static_cast<T>(1)/y : y;
}


//EXERCICE 3, question 2
// exp(x) = 1 + x(  1 + x/2 ( 1 + x/3 ( 1 + ..) ) )
template<typename T>
T horner(const T &x){
    const T x_abs = std::abs(x);
    T y = static_cast<T>(1);
    const unsigned int nbIt = 21; // pas comme dit dans l'énoncé
    for (int i=nbIt; i>0; --i){
        y = static_cast<T>(1) + y * x_abs / (i);
    }
    return (x < static_cast<T>(0)) ? static_cast<T>(1)/y : y;
}


//EXERCICE 3, question 5
template<typename T>
T reduced_horner(const T &x) {
    int k = static_cast<int> (x / M_LN2 - M_LN2 / 2);
    float r = x - M_LN2 * k;
    T y = horner(r) * pow(2,k);
    return y;
}


// https://nic.schraudolph.org/pubs/Schraudolph99.pdf
double exp_fast(const double &x){

    long int y = ((long int) (1512775 * x + 1072632447)) << 32;

    return * ( double * ) & (y);
}

template<typename T>
T relative_error(const T& estimated, const T& expected){
    return std::abs( (estimated - expected ) / expected );
}

void print_data(const std::string methodName, const double precision, const double time){

    std::cout << methodName 
              << " : [mean error: " << std::fixed  << std::internal << std::setw(7) << std::setprecision(4) << std::setfill('0') << precision << " % ]"
              << "\t [time: "  << std::fixed << std::setprecision(4) << time << " s]" << std::endl;
}


double test_precision(std::function<double(const double&)> f, const unsigned seed, const unsigned int nbRuns, const double range){

    std::default_random_engine generator(seed);
    std::uniform_real_distribution<double> distrib(-range,range);

    long double sum = 0;
    for(unsigned int i=0; i<nbRuns; ++i){
        double x = distrib(generator);
        sum += relative_error<double>(f(x),exp(x));
    }
   
    return 100 * sum / nbRuns;
}


int main()
{
    // random setup
    const unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);

    // distribution
    const int max_range_double = 20; //709; // exp(x) should fit in a double
    std::uniform_real_distribution<double> distrib(-max_range_double,max_range_double);

    // computation options
    const unsigned int nbRuns = 1.0e7;
  
    //////////////////////////////////////////////////////////////
    // time tests

    // empty version
    generator.seed(seed);
    auto start = std::chrono::system_clock::now();
    for(unsigned int i=0; i<nbRuns; ++i){
        const volatile double value = distrib(generator);
    }
    auto elapsed = std::chrono::system_clock::now() - start;
    std::chrono::duration<double> elapsed_empty = elapsed;

    // cmath version
    generator.seed(seed);
    start = std::chrono::system_clock::now();
    for(unsigned int i=0; i<nbRuns; ++i){
        const volatile double value = exp(distrib(generator));
    }
    elapsed = std::chrono::system_clock::now() - start;
    std::chrono::duration<double> elapsed_cmath = elapsed;

    // taylor version
    generator.seed(seed);
    start = std::chrono::system_clock::now();
    for(unsigned int i=0; i<nbRuns; ++i){
        const volatile double value = taylor(distrib(generator));
    }
    elapsed = std::chrono::system_clock::now() - start;
    std::chrono::duration<double> elapsed_taylor = elapsed;

    // horner version
    generator.seed(seed);
    start = std::chrono::system_clock::now();
    for(unsigned int i=0; i<nbRuns; ++i){
        const volatile double value = horner(distrib(generator));
    }
    elapsed = std::chrono::system_clock::now() - start;
    std::chrono::duration<double> elapsed_horner = elapsed;

    // reduced horner version
    generator.seed(seed);
    start = std::chrono::system_clock::now();
    for(unsigned int i=0; i<nbRuns; ++i){
        const volatile double value = reduced_horner(distrib(generator));
    }
    elapsed = std::chrono::system_clock::now() - start;
    std::chrono::duration<double> elapsed_reduced_horner = elapsed;

    // fast version
    generator.seed(seed);
    start = std::chrono::system_clock::now();
    for(unsigned int i=0; i<nbRuns; ++i){
        const volatile double value = exp_fast(distrib(generator));
    }
    elapsed = std::chrono::system_clock::now() - start;
    std::chrono::duration<double> elapsed_fast = elapsed;



    //////////////////////////////////////////////////////////////
    // precision tests

    double precision_cmath = 0;
    double precision_taylor = test_precision(taylor<double>, seed, nbRuns, max_range_double);
    double precision_horner = test_precision(horner<double>, seed, nbRuns, max_range_double);
    double precision_reduced_horner = test_precision(reduced_horner<double>, seed, nbRuns, max_range_double);
    double precision_fast   = test_precision(exp_fast, seed, nbRuns, max_range_double);


    //////////////////////////////////////////////////////////////
    // print results

    print_data("cmath     ", precision_cmath,  (elapsed_cmath  - elapsed_empty).count());
    print_data("taylor    ", precision_taylor, (elapsed_taylor - elapsed_empty).count());
    print_data("horner    ", precision_horner, (elapsed_horner - elapsed_empty).count());
    print_data("red horner", precision_reduced_horner, (elapsed_reduced_horner - elapsed_empty).count());
    print_data("fast      ", precision_fast,   (elapsed_fast   - elapsed_empty).count());
    std::cout << std::endl;



    return 0;
}

