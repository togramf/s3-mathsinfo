#include <iostream>
#include <cstdlib>
#include <Eigen/Dense>
#include <chrono>
#include <cassert>

using namespace Eigen;


// mac   : g++ -Wall -O2 -std=c++11 -I /usr/local/include/eigen3 -fopenmp  strassen.cpp 
// linux : g++ -Wall -O2 -std=c++11 -I /usr/include/eigen3 -fopenmp  strassen.cpp -o strassen




MatrixXd matrixProductLoop(const MatrixXd &A, const MatrixXd &B)
{
  MatrixXd C = MatrixXd::Zero(A.rows(),B.cols());
  for(int i=0; i<C.rows(); ++i)
    for(int j=0; j<C.cols(); ++j){
      for(int k=0; k<A.cols(); ++k)
        C(i,j) += A(i,k)*B(k,j);
    }
  
  return C;
}



MatrixXd strassen(const MatrixXd &A, const MatrixXd &B)
{
  // if A or B too small
  if(A.rows() < 64) return matrixProductLoop(A,B);
  
  const unsigned int rows = A.rows()/2;
  const unsigned int cols = A.cols()/2;
  
  MatrixXd P1(strassen(A.topLeftCorner(rows,cols) , B.topRightCorner(rows,cols)-B.bottomRightCorner(rows,cols)));     // a(f-h)
  MatrixXd P2(strassen(A.topLeftCorner(rows,cols) + A.topRightCorner(rows,cols) , B.bottomRightCorner(rows,cols)));   // (a+b)h
  MatrixXd P3(strassen(A.bottomLeftCorner(rows,cols) + A.bottomRightCorner(rows,cols),B.topLeftCorner(rows,cols)));   // (c+d)e
  MatrixXd P4(strassen(A.bottomRightCorner(rows,cols), B.bottomLeftCorner(rows,cols)-B.topLeftCorner(rows,cols)));    // d(g-e)
  MatrixXd P5(strassen(A.topLeftCorner(rows,cols)+A.bottomRightCorner(rows,cols),B.topLeftCorner(rows,cols)+B.bottomRightCorner(rows,cols)));     // (a+d)(e+h)
  MatrixXd P6(strassen(A.topRightCorner(rows,cols)-A.bottomRightCorner(rows,cols),B.bottomLeftCorner(rows,cols)+B.bottomRightCorner(rows,cols))); // (b-d)(g+h)
  MatrixXd P7(strassen(A.topLeftCorner(rows,cols)-A.bottomLeftCorner(rows,cols),B.topLeftCorner(rows,cols)+B.topRightCorner(rows,cols)));         // (a-c)(e+f)
 
  MatrixXd C(A.rows(),A.cols());
  C.topLeftCorner(rows,cols)     = P5 + P4 - P2 + P6;
  C.topRightCorner(rows,cols)    = P1 + P2;
  C.bottomLeftCorner(rows,cols)  = P3 + P4;
  C.bottomRightCorner(rows,cols) = P1 + P5 - P3 - P7;
  
  return C;
}



int main()
{
  srand(time(0)); // choose a seed for the random numbers

  const unsigned int matrixSize = 2048;
  const unsigned int nbRuns = 4;


  // 2 random matrices
  MatrixXd A = MatrixXd::Random(matrixSize,matrixSize); 
  MatrixXd B = MatrixXd::Random(matrixSize,matrixSize); 
  MatrixXd C = MatrixXd::Random(matrixSize,matrixSize); 


#if 1 // eigen multithread, put 0 if openMP is not supported, 1 otherwize
  const int nbProc = 8; // choose accordingly to the number of proc on your machine 
  Eigen::setNbThreads(nbProc);
  auto start_multi = std::chrono::high_resolution_clock::now();
  for(unsigned int i=0; i<nbRuns; ++i){
    C = A*B;
  }
  auto elapsed_multi = std::chrono::high_resolution_clock::now() - start_multi;
  std::cout << "eigen Multi : " << 0.001 * std::chrono::duration_cast<std::chrono::milliseconds>(elapsed_multi).count() << " s" << std::endl;
  Eigen::setNbThreads(1);
#endif

  // eigen  
  auto start = std::chrono::high_resolution_clock::now();
  for(unsigned int i=0; i<nbRuns; ++i){
    C = A*B;
  }
  auto elapsed = std::chrono::high_resolution_clock::now() - start;
  std::cout << "eigen       : " << 0.001 * std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count() << " s" << std::endl;
  
  // strassen
  start = std::chrono::high_resolution_clock::now();
  for(unsigned int i=0; i<nbRuns; ++i){
    MatrixXd C = strassen(A,B); 
  }
  elapsed = std::chrono::high_resolution_clock::now() - start;
  std::cout << "strassen    : " << 0.001 * std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count() << " s" << std::endl;

  // 3 loops  
  start = std::chrono::high_resolution_clock::now();
  for(unsigned int i=0; i<nbRuns; ++i){
    MatrixXd C = matrixProductLoop(A,B);
  }
  elapsed = std::chrono::high_resolution_clock::now() - start;
  std::cout << "3 loops     : " << 0.001 * std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count() << " s" << std::endl;

  return 0;
}




// result 2048x2048
// strassen (>128) : 164 s
// eigen : 174 s
// standard : 2900 s
// thread : 558 s

// result 1024x1024
// strassen (>128) : 23 s
// eigen : 22 s
// standard : 276 s
// thread : 69 s

// result 512x512
// strassen (>128) : 3.2 s
// eigen : 2.8 s
// standard : 32 s
// thread : 8.6 s
