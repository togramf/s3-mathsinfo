#include <iostream>
#include <random>
#include <iomanip>
#include <chrono>
#include <cmath>

//Exo1 : Calcul de pi 

// g++ -Wall -std=c++11 exo1.cpp -o exo1

const long double PI = 3.14159265358979323846264338327950288419716939937510582097494459230781640628620899862803482534211706798214808651L; // 110 first digit

template<typename T>
T pi_template (const unsigned int nbIter){

    // select a random generator engine an a distribution 
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937 generator(seed);
    std::uniform_real_distribution<T> uniformRealDistribution(0,1);

    unsigned int nbPointsInCircle = 0; 

    // start launching points
    for(unsigned int i=0; i<nbIter; i++){
        const T x = uniformRealDistribution(generator);
        const T y = uniformRealDistribution(generator);

        if(x*x + y*y < static_cast<T>(1.0))
            nbPointsInCircle++; 
    }

    return 4.0 * nbPointsInCircle / static_cast<T>(nbIter);
}

int main () {
    const unsigned int nbIter = 2000000;
    std::cout << "Estimation du nombre PI  : " << std::setprecision(20) << pi_template<long double>(nbIter) << std::endl;
    std::cout << "Erreur de notre fonction : " << std::setprecision(20) << std::abs(pi_template<long double>(nbIter) - M_PI) << std::endl;
    std::cout << "Nombre PI réel           : " << std::setprecision(20) << M_PI << std::endl;

    return 0; 
}