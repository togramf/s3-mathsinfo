#include <iostream>
#include <random>
#include <chrono>

// Exo3 : Jeu de Dés 

// g++ -Wall -std=c++11 exo2_part1.cpp -o exo2_1

int manche (const int D_1[], const int D_2[]){
    // select seed from time, a generator
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937 generator(seed);
    std::uniform_int_distribution<int> uniformIntDistribution(1,6);

    int d1 = D_1[uniformIntDistribution(generator)];
    int d2 = D_2[uniformIntDistribution(generator)];
    if (d1>d2)
        return 1;
    else 
        return 2;
}

std::vector<unsigned int> partie (const int D_1[], const int D_2[], int nbManches){
    std::vector<unsigned int> result = {0,0};
    int victoireA = 0; 
    int victoireB = 0; 

    for (int i=0; i<nbManches; i++){
        int result = manche (D_1, D_2);
        if (result == 1)
            victoireA++;
        else 
            victoireB++;
    }

    result[0]=victoireA;
    result[1]=victoireB; 
    return result;
}

int main(){
    int A[] = {3, 3, 3, 3, 3, 6}; //1
    int B[] = {2, 2, 2, 5, 5, 5}; //2
    int C[] = {1, 4, 4, 4, 4, 4}; //3

    int nbManches = 100000;

    // Manches A contre B
    std::vector<unsigned int> a_b = partie (A, B, nbManches);
    int ptA_B = a_b[0];
    int ptB_A = a_b[1];

    std::cout << "score A : "<< ptA_B << " score B : " << ptB_A << std::endl;

    // Manches A contre C
    std::vector<unsigned int> a_c = partie (A, C, nbManches);
    int ptA_C = a_c[0];
    int ptC_A = a_c[1];

    std::cout << "score A : "<< ptA_C << " score C : " << ptC_A << std::endl;
    
    // Manches B contre C
    std::vector<unsigned int> b_c = partie (B, C, nbManches);
    int ptB_C = a_c[0];
    int ptC_B = a_c[1];

    std::cout << "score B : "<< ptB_C << " score C : " << ptC_B << std::endl;
    

    
}