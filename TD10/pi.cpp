#include <iostream>
#include <iomanip>
#include <cmath>


const long double PI = 3.14159265358979323846264338327950288419716939937510582097494459230781640628620899862803482534211706798214808651L; // 110 first digit

template<typename T>
T bad_pi(const unsigned int & n){
	return T(3.14);
}


int main(){

	const unsigned int nb_iterations_pi = 10;
	std::cout << "Bad pi : " << bad_pi<long double>(nb_iterations_pi) << std::endl;
	std::cout << std::setprecision(30) << "error : " << PI - bad_pi<long double>(nb_iterations_pi) << std::endl;

	return 0;
}
