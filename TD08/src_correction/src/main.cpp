#include <iostream>
#include <functional>
#include <cmath>
#include <cassert>
#include <iomanip>


double trapeze(std::function<double(const double&)> &f, 
               const double &lowerBound, 
               const double &upperBound,
               const int NbIntervals){

    const double step = (upperBound - lowerBound) / NbIntervals;
    double sum = 0.0;

    for(unsigned int i=1; i<NbIntervals; ++i)
        sum += f(lowerBound +i*step);

    sum *= 2.0;
    sum += f(lowerBound) + f(upperBound);

    return 0.5 * step * sum;
}


double simpson(std::function<double(const double&)> &f, 
               const double &lowerBound, 
               const double &upperBound,
               const int NbIntervals){

    assert(NbIntervals%2 == 0);
    double step = (upperBound - lowerBound) / NbIntervals;

    double sum1 = 0.0;
    for(unsigned int i=1; i<NbIntervals; i+=2)
        sum1 += f(lowerBound +i*step);

    double sum2 = 0.0;
    for(unsigned int i=2; i<NbIntervals-1; i+=2)
        sum2 += f(lowerBound +i*step);

    return (f(lowerBound) + f(upperBound) + 4.0*sum1 + 2.0*sum2) * step / 3.0;
}


void mainTrapezeSimpson()
{
    std::function<double(const double&)> f, F;
    // f = [](const double&x){return 3*x*x + 2*x -1 ;};  // 3x^2 + 2x -1
    // F = [](const double&x){return x*x*x + x*x -x ;};  //  x^3 + x^2 -x 
    f = [](const double&x){return (16.0*x*x*x - 42.0*x*x + 2.0*x) / (sqrt(-16.0*std::pow(x,8) + 112.0*std::pow(x,7) - 204.0*std::pow(x,6) + 28.0*std::pow(x,5) -x*x*x*x + 1.0))  ;}; 
    F = [](const double&x){return asin(4.0*x*x*x*x -14.0*x*x*x + x*x) ;}; 

    const unsigned int nbSubdivisions = 100;
    const double lowerBound = -0.2;
    const double upperBound =  0.3;
    const int nbDigits = 20;

    std::cout << "nb subdivisions   : " << nbSubdivisions << std::endl;
    std::cout << "trapèzes          : " << std::setprecision(nbDigits) << trapeze(f,lowerBound, upperBound, nbSubdivisions) << std::endl;
    std::cout << "simpson           : " << std::setprecision(nbDigits) << simpson(f,lowerBound, upperBound, nbSubdivisions) << std::endl;
    std::cout << "solution exacte   : " << std::setprecision(nbDigits) << F(upperBound) - F(lowerBound) << std::endl;
    std::cout << std::endl << std::endl;
}


double gaussLegendre3pts(std::function<double(const double&)> &f)
{
    const double A0 = 5.0 / 9.0;
    const double A1 = 8.0 / 9.0;
    const double A2 = 5.0 / 9.0;

    const double x0 = - sqrt(3.0 / 5.0);
    const double x1 =   0.0;
    const double x2 =   sqrt(3.0 / 5.0);

    return A0*f(x0) + A1*f(x1) + A2*f(x2);
}


void mainGaussLegendre()
{
    std::function<double(const double&)> f, g;
    f = [](const double&x){return -x*x*x*x + 2.0*x*x*x + 3.0 ;};  // -x^4 + 2x^3 +3
    g = [](const double&x){return -x*x*x*x + 2.0*x*x*x + 3.0 + 0.2*cos(3.0*M_PI*x) ;};  //  f(x) + 0.2 cos(3 pi x)

    const unsigned int nbSubdivisions = 100;
    const int nbDigits = 20;

    std::cout << "integral exacte f : " << std::setprecision(nbDigits) << 5.6 << std::endl;
    std::cout << "Gauss-Legendre f  : " << std::setprecision(nbDigits) << gaussLegendre3pts(f) << std::endl;
    std::cout << "trapèzes f        : " << std::setprecision(nbDigits) << trapeze(f,-1.0, 1.0, nbSubdivisions) << std::endl;
    std::cout << "simpson f         : " << std::setprecision(nbDigits) << simpson(f,-1.0, 1.0, nbSubdivisions) << std::endl << std::endl;

    std::cout << "integral exacte g : " << std::setprecision(nbDigits) << 5.6 + (sin(-3.0*M_PI) - sin(3.0*M_PI)) * 0.2/(3.0*M_PI) << std::endl;
    std::cout << "Gauss-Legendre g  : " << std::setprecision(nbDigits) << gaussLegendre3pts(g) << std::endl;
    std::cout << "trapèzes g        : " << std::setprecision(nbDigits) << trapeze(g,-1.0, 1.0, nbSubdivisions) << std::endl;
    std::cout << "simpson g         : " << std::setprecision(nbDigits) << simpson(g,-1.0, 1.0, nbSubdivisions) << std::endl << std::endl;
}



double euler(std::function<double(const double&, const double&)> &f,
             const double &a,
             const double &b,
             const double &ya,
             const double &dx)
{
    double y = ya;
    double x = a;

    while(x<b){
        y += dx*f(x,y);
        x += dx;
    }

    return y;
}


double eulerAmeliore(std::function<double(const double&, const double&)> &f,
                     const double &a,
                     const double &b,
                     const double &ya,
                     const double &dx)
{
    double y = ya;
    double x = a;

    while(x<b){
        const double y_tmp = y + dx*f(x,y);
        y += dx * 0.5 * (f(x,y) + f(x+dx,y_tmp));
        x += dx;
    }

    return y;
}


double rungeKutta(std::function<double(const double&, const double&)> &f,
                  const double &a,
                  const double &b,
                  const double &ya,
                  const double &dx)
{
    double y = ya;
    double x = a;

    while(x<b){
        const double y_tmp = y + 0.5*dx*f(x,y);
        y += dx * f(x+0.5*dx,y_tmp);
        x += dx;
    }

    return y;
}


void mainEuler()
{
    // ODE : y' -y/x = x^2  <=> f(x,y) = x^2 +y/x
    std::function<double(const double&, const double&)> f = [](const double&x, const double&y){return x*x + (y/x);}; 

    // solution y(x) = Cx + x^3 / 2
    constexpr double C  = 3.5; 
    std::function<double(const double&)> g = [](const double&x){return C*x + 0.5*x*x*x;}; 

    // for a=1
    constexpr double ya = 4.0; // y(a=1)=4
    const double a = 1.0;
    const double b = 15.0;
    const double dx = 0.5;

    const int nbDigits = 10;
    std::cout << "Euler(" << b << ")   = " << std::setprecision(nbDigits) << euler(f, a, b, ya, dx) << std::endl; 
    std::cout << "EulerA(" << b << ")  = " << std::setprecision(nbDigits) << eulerAmeliore(f, a, b, ya, dx) << std::endl; 
    std::cout << "Runge-K(" << b << ") = " << std::setprecision(nbDigits) << rungeKutta(f, a, b, ya, dx) << std::endl; 
    std::cout << "y(" << b << ")       = " << std::setprecision(nbDigits) << g(b) << std::endl; 
}


int main()
{
    mainTrapezeSimpson();

    mainGaussLegendre();

    mainEuler();

    return 0;
}

