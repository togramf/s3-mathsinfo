#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <iostream>
#include <complex>
#include <cmath>
#include <random>
#include <chrono>

using namespace std::complex_literals;  // to use 'i' as a complex


void my_julia(cv::Mat &image, 
           const std::complex<double> &c,
           const unsigned int nb_iteration,
           const double upper_threshold){

  // starting complex number
  std::cout << "c = " << c << std::endl;

  // compute the fracatal for each pixel
  for(int i=0; i<image.rows; ++i)
    for(int j=0; j<image.cols; ++j){

      // pixel to complex
      const double image_scale = 1.5;
      std::complex<double> z = image_scale * ((i * 2.0 / double(image.rows-1)) -1.0)
                             + image_scale * ((j * 2.0 / double(image.cols-1)) -1.0)*1i;
      // std::cout << "z : " << z << std::endl;

      // compute number of runs for a pixel
      // here ...
      unsigned int nb_runs = 0; 
      bool stop = false;
      
      while (!stop){
        // different transfert functions
        // z = z*z + c;
        // z = c*std::cos(z);
        // z = c*c+z;
        z = std::sin(z)/c;
        if (std::abs(z)<upper_threshold)
          nb_runs++;
        else 
          stop = true;
      }

      // assign the correct color to a pixel
      image.at<unsigned char>(i,j) = 255 * nb_runs / nb_iteration;
    } 
}

//correction
void julia(cv::Mat &image, 
           const std::complex<double> &c,
           const unsigned int nb_iteration,
           const double upper_threshold){

  // starting complex number
  std::cout << "c = " << c << std::endl;

  // compute the fracatal for each pixel
  for(int i=0; i<image.rows; ++i)
    for(int j=0; j<image.cols; ++j){

      // pixel to complex
      const double image_scale = 1.5;
      std::complex<double> z = image_scale * ((i * 2.0 / double(image.rows-1)) -1.0)
                             + image_scale * ((j * 2.0 / double(image.cols-1)) -1.0)*1i;
      //std::cout << "z : " << z << std::endl;

      // compute number of runs for a pixel
      unsigned int nb_runs = 0;
      for(unsigned int iter=0; iter<nb_iteration; ++iter){
        // z = z * z + c;
        // z = c*std::sin(z);
        // z = c*std::cos(z);
        // z = sin(z)*cos(z)/c;
        z = z  + std::cos(z) * c ;
        nb_runs++;
        if(std::sqrt(std::norm(z)) > upper_threshold)
          break;
      }

      // assign the correct color to a pixel
      image.at<unsigned char>(i,j) = 255 * nb_runs / double(nb_iteration-1);
    } 
}

//////////////////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char ** argv)
{

  // create empty image
  constexpr unsigned int image_width  = 800;
  constexpr unsigned int image_height = 600;
  cv::Mat image(image_width,image_height,CV_8UC1, cv::Scalar(0));


  // random generator
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::mt19937 generator(seed);
  std::uniform_real_distribution<double> distrib(-1.0, 1.0);


  // make a complex fractal (Julia set)  ///////////////////////
  // julia(image, 0.266013 -0.003359i, 1000, 20);
  // julia(image, 0.32 -0.05, 100, 60);
  // julia(image, 0.223549 -0.548997i, 100, 60);
  // julia(image, 0.156988 +0.592612, 100, 60);
  // julia(image, 0.341446 -0.378511, 50, 1000);
  julia(image, std::complex<double>(distrib(generator),distrib(generator)), 20, 200);
  // julia(image, std::complex<double>(distrib(generator),distrib(generator)), 100, 500);


  // display the image /////////////////////////////////////////
  cv::namedWindow("image");
  cv::moveWindow("image",1000,100);
  std::cout << "appuyer sur une touche ..." << std::endl;
  cv::imshow("image", image);
  cv::waitKey();


  // color map : https://docs.opencv.org/4.x/d3/d50/group__imgproc__colormap.html
  cv::applyColorMap(image, image, cv::COLORMAP_TWILIGHT_SHIFTED);
  // cv::COLORMAP_PINK
  // cv::COLORMAP_AUTUMN 
  // cv::COLORMAP_BONE 
  // cv::COLORMAP_JET 
  // cv::COLORMAP_WINTER 
  // cv::COLORMAP_RAINBOW 
  // cv::COLORMAP_OCEAN 
  // cv::COLORMAP_SUMMER 
  // cv::COLORMAP_SPRING 
  // cv::COLORMAP_COOL 
  // cv::COLORMAP_HSV 
  // cv::COLORMAP_PINK 
  // cv::COLORMAP_HOT 
  // cv::COLORMAP_PARULA 
  // cv::COLORMAP_MAGMA 
  // cv::COLORMAP_INFERNO 
  // cv::COLORMAP_PLASMA 
  // cv::COLORMAP_VIRIDIS 
  // cv::COLORMAP_CIVIDIS 
  // cv::applyColorMap(image, image, cv::COLORMAP_TWILIGHT_SHIFTED ); 
  // cv::COLORMAP_TWILIGHT 
  // cv::COLORMAP_TWILIGHT_SHIFTED 
  // cv::COLORMAP_TURBO 
  // cv::COLORMAP_DEEPGREEN 

  // display the image 
  std::cout << "appuyer sur une touche ..." << std::endl;
  cv::imshow("image", image);
  cv::waitKey();

  // save image
  cv::imwrite("le_meilleur_tp_de_l_année.png",image);

  return 1;
}
